# Monitoring with Prometheus and Grafana

This Project deploys a monitoring stack based on `prometheus` and `grafana`

## Usage

adjust the `inventory/hosts.yml` and the files in `inventory/group_vars` to your needs.

deploy all functions with:

```
ansible-playbook playbook.yml
```

deploy only prometheus:

```
ansible-playbook playbook.yml --tags prometheus
```

change only prometheus config:

```
ansible-playbook playbook.yml --tags prometheus_config
```

deploy only node_exporter:

```
ansible-playbook playbook.yml --tags node_exporter
```

deploy only grafana:

```
ansible-playbook playbook.yml --tags grafana
```
